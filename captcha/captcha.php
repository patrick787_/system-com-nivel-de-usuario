<?php
session_start();
$image  = imagecreate(120,30);
$codigo = md5(rand());
$letras = substr($codigo,0,6);

$fundo    = imagecolorallocate($image,255,255,255); //cor do texto
$corfonte = imagecolorallocate($image,55,55,55); //cor do background

$_SESSION['codigoCaptcha'] = $letras;

imagefill($image,0,0,$corfonte);

imagettftext($image,20,0,15,25,$fundo,'futura.ttf',$letras);

imagejpeg($image);
imagedestroy($image);

?>