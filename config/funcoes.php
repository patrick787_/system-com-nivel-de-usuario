<?php

//FUNCAO PARA VALIDAR EMAIL
function ValidarEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

//FUNCAO PARA CHECAR DIRETORIO
function checkDir($dir) {
    if (file_exists($dir) && is_dir($dir)):
        return true;
    else:
        return false;
    endif;
}

//FUNCAO PARA LOGAR NO SISTEMA
function logar() {
    if (empty($_SESSION['autUser'])):
        header('Location:' . SYSTEM . '/index.php?exe=restrito');
    else:
        
        $userId = $_SESSION['autUser']['id']; //ARMAZENA O ID DO USUARIO
        $read = read(TAB_USERS, "WHERE id='$userId'"); //RECUPERA OS DADOD DO USUARIO
        //foreach ($read as $user); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
        $user = mysqli_fetch_array($read);

        //CONDICAO PARA INDENTIFICAR E REDIRECIONAR O USUARIO NO SISTEMA
        if ($user['nivel'] == 1):
            header('Location:' . SYSTEM . '/system/admin-master/index.php');
        elseif ($user['nivel'] == 2):
            header('Location:' . SYSTEM . '/system/admin/index.php');
        elseif ($user['nivel'] == 3):
            header('Location:' . SYSTEM . '/system/moderador/index.php');
        else:
            unset($_SESSION['autUser']);
            header('Location:' . SYSTEM . '/system/index.php?exe=permissao');
        endif;
    endif;
}

//FUNCAO PARA ATUALIZAR O TEMPO DO USUARIO NO SISTEMA
function starTime() {
    $userId = $_SESSION['autUser']['id'];//ARMAZENA O ID DO USUARIO
    
    $readTime = read(TAB_USERS, "WHERE id = '$userId'");//RECUPERA OS DADOS DO USUARIO
    //foreach ($readTime as $userTime); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
    $userTime = mysqli_fetch_array($readTime);

    $dataAtual = date("Y-m-d H:i:s");

    if ($dataAtual > $userTime['log_in_time']):
        $dataTime = array('log' => '0', 'log_out' => date("Y-m-d H:i:s"));
        update(TAB_USERS, $dataTime, "id = '$userId'");

        unset($_SESSION['autUser']);
        header('Location:' . SYSTEM . '/index.php');
    else:
        $dataTime = array('log_in_time' => date("Y-m-d H:i:s", strtotime('+1 hour')));
        update(TAB_USERS, $dataTime, "id = '$userId'");
    endif;
}

//FUNCAO PARA ENVIO DE E-MAIL
function enviarEmail($assunto, $mensagem, $remetente, $nomeRemetente, $destino, $nomeDestino) {

    require_once('phpmailer/class.phpmailer.php'); //Include os arquivos do PHPMailer

    $mail = new PHPMailer(); //inicia a classe do PHPMailer
    $mail->IsSMTP(); //Habilita envio SMPT
    $mail->SMTPAuth = true; //Ativa email autenticado
    $mail->IsHTML(true); //html

    $mail->Host = MAILHOST; //Servidor de envio
    $mail->Port = MAILPORT; //Porta de envio
    $mail->Username = MAILUSER; //email para smtp autenticado
    $mail->Password = MAILPASS; //seleciona a senha para envio

    $mail->From = utf8_decode($remetente); //remtente
    $mail->FromName = utf8_decode($nomeRemetente); //nome do remetetene

    $mail->Subject = utf8_decode($assunto); //assunto da mensagem
    $mail->Body = utf8_decode($mensagem); //mensagem
    $mail->AddAddress(utf8_decode($destino), utf8_decode($nomeDestino)); //email e nome do destino

    if ($mail->Send()):
        return true;
    else:
        return false;
    endif;
}

//FUNCAO PARA MOSTRAR O STATUS
function FuncStatus($Status = null) {
    $FncStatus = [
        1 => 'Ativo',
        2 => 'Bloqueado'
    ];

    if (!empty($Status)):
        return $FncStatus[$Status];
    else:
        return $FncStatus;
    endif;
}

//FUNCAO PARA MOSTRAR O NIVEL DO USUARIO
function FuncNivel($Nivel = null) {
    $FncNivel = [
        1 => 'Master',
        2 => 'Admin',
        3 => 'Moderador'
    ];

    if (!empty($Nivel)):
        return $FncNivel[$Nivel];
    else:
        return $FncNivel;
    endif;
}

//FUNCAO PARA EXECUTAR E SALVAR O LOG DO SUAURIO NO SISTEMA
function SaveLog() {
    $userId = $_SESSION['autUser']['id'];

    //OBTEM A URL
    $dominio = $_SERVER['HTTP_HOST'];
    $url = "http://" . $dominio . $_SERVER['REQUEST_URI'];
    $urlAtual = $url;

    //ARMAZENA OS DADOS EM UM ARRAY
    $log = array(
        'log_url' => $urlAtual,
        'log_data' => date("Y-m-d H:i:s"),
        'log_userid' => $userId,
        'log_hostname' => gethostbyaddr($_SERVER['REMOTE_ADDR'])
    );

    //CADASTRA O LOG NO BANCO
    create(TAB_LOG, $log);
}
