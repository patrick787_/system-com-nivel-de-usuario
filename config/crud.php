<?php

require_once('config.php');

//FUNCAO PARA CONEXAO COM O BANCO DE DADOS
function connect(){
    $link = mysqli_connect(HOST, USER, PASS, BASE);
    mysqli_set_charset($link, CHARSET) or die(mysqli_error());
    if ($link):
        return $link;
    else:
        echo'erro na conexao' . mysqli_error($link);
    endif;
}

//FUNCAO PARA FECHAR A CONEXAO COM O BANCO DE DADOS
function close($conexao){
    mysqli_close($conexao) or die(mysqli_error($conexao));
}

//FUNCAO PARA EXECUTAR AS QUERYS
function executeQuery($query){
    $conexao = connect();
    $result = mysqli_query($conexao, $query) or die(mysqli_error($conexao));
    close($conexao);
    return $result;
}

//FUNCAO PARA CADASTRAR NO BANCO DE DADOS
function create($tabela, array $data){
    $campos = implode(", ", array_keys($data));
    $valor = "'" . implode("', '", array_values($data)) . "'";
    $qrCreate = "INSERT INTO {$tabela} ($campos) VALUES ($valor)";       
    $create = executeQuery($qrCreate);
    if ($create):
        return true;
	else:
		echo'erro ao cadastrar'. mysqli_error($conexao);
    endif;
}

//FUNCAO PARA LER DADOS NO BANCO
function read($tabela, $cond = NULL){
    $qrRead = "SELECT * FROM {$tabela} {$cond}";
    $read = executeQuery($qrRead);
    if ($read):
        return $read;  
	else:
		echo'erro ao ler'. mysqli_error($conexao);
    endif;
}

//FUNCAO PARA EDITAR DADOS NO BANCO
function update($tabela, array $data, $where){
    foreach ($data as $fields => $values):
        $campos[] = "$fields = '$values'";
    endforeach;

    $campos = implode(", ", $campos);
    $qrUpdate = "UPDATE {$tabela} SET $campos WHERE {$where}";    
    $update = executeQuery($qrUpdate);
    if ($update):
        return true;    
	else:
		echo'erro ao atualizar'. mysqli_error($conexao);
    endif;
}

//FUNCAO PARA DELTAR DADOS NO BANCO
function delete($tabela, $where){
    $qrDelete = "DELETE FROM {$tabela} WHERE {$where}";
    $delete = executeQuery($qrDelete);
    if ($delete):
        return true;  
	else:
		echo'erro ao deletar'. mysqli_error($conexao);
    endif;
}