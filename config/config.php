<?php
error_reporting('E_ALL & ~E_NOTICE');
date_default_timezone_set("America/Sao_Paulo");


//DEFINE OS DADOS PARA CONEXÃO COM O BANCO
define('HOST', 'localhost');
define('USER', 'root');
define('PASS', '');
define('BASE', 'system');
define('CHARSET', 'UTF8');

//DEFINE NOME DO SITE
define('SITE', 'SYSTEM COM LOGIN E SENHA');

//DEFINE SERVIDOR PARA ENVIO DE E-MMAIL
define('MAILUSER', 'EMAIL');
define('MAILPASS', 'SENHA');
define('MAILPORT', 'PORTA');
define('MAILHOST', 'HOST');

//DEFINE O LINK PARA URL DO SISTEMA
define('SYSTEM', ''); //REMOVER (/) NO FINAL EX: https://www.sysoeducation.com.br

//DEFINE AS MENSAGEM PADRÕES DO SISTEMA
define('C_BRANCOS', '<p class="ms no">Oppss! Existe campos em brancos. Por favor, Preencha todos os campos para continuar!</p>');
define('C_SALVO', '<span class="ms ok">Pronto! Dados foram salvo com sucesso!</span>');
define('C_DELETADO', '<span class="ms ok">Pronto! Dados foram removido com sucesso!</span>');
define('C_EDITADO', '<span class="ms ok">Pronto! Dados foram editado com sucesso!</span>');
define('C_NAO_EXISTE', '<span class="ms no">Oppss! Dados não cadastrados. Por favor, volte mais tarde!</span>');
define('C_EMAIL_INVALIDO', '<span class="ms no">Oppss! E-mail inválido. Por favor, coloque um e-mail válido!</span>');

//DEFINE AS TABELAS DO BANCO DE DADOS
define('TAB_USERS', 'users');
define('TAB_POSTS', 'posts');
define('TAB_CATEGORIA', 'categoria');
define('TAB_MANUTENCAO', 'manutencao');
define('TAB_VIDEOS', 'videos');
define('TAB_LOG', 'log');
define('TAB_CODE', 'recover');

//DEFINE OS REDIRECINAMENTO DAS PAGINAS
define('R_REFRESHHOME', 'index.php');
define('R_REFRESHUSERS', 'index.php?exe=users');
define('R_REFRESHPOSTS', 'index.php?exe=posts');
define('R_REFRESHMANUTENCAO', 'index.php?exe=manutencao');
define('R_REFRESHVIDEOS', 'index.php?exe=videos');