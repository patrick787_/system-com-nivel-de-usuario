<?php
ob_start();
session_start();
require_once('config/crud.php');

//MODO DE MANUTENCAO
if (empty($_SESSION['autUser'])):
    $read = read('manutencao');
    //foreach ($read as $rows); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
    $rows = mysqli_fetch_array($read);

    if ($rows['status'] != 1 && empty($_SESSION['autUser'])):
        require('manutencao.php');
        die;
    endif;
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">  
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title><?= SITE; ?></title>
        <meta name="description" content="<?= SITE; ?>"/>
        <meta name="robots" content="index, follow"/>  
        <meta itemprop="name" content="<?= SITE; ?>"/>
        <meta itemprop="description" content="<?= SITE; ?>"/>
        <meta itemprop="url" content="<?= SYSTEM; ?>/<?= SYSTEM; ?>"/>
    </head>
    <body>
        <style>
            * {margin: 0 auto; padding: 0;}
            .system_content{display: block; background: #2a4174; position: fixed; width: 100%; height: 100%;}
            .system_content .box{display: block; width: 600px; margin: 10% auto; max-width: 90%; background: #fff; padding: 50px;}
            .system_content .box h1{font-size: 2em; font-weight: 600; color: #000; text-shadow: 1px 1px 0 #eee; text-align:center;}
            .system_content .box p{margin: 15px 0; text-align:center;}
        </style>
        <article class="system_content">
            <div class="box">
                <h1>SEJA BEM-VINDO!</h1>
                <p>Estamos trabalhando para melhorar nosso site.</p>
                <p><strong>Por favor, volte em algumas horas para conferir as novidades!</strong></p>
                <p><strong>Atenciosamente:</strong> <?= SITE; ?></p>
            </div>
        </article>

    </body>
</html>
<?php
ob_end_flush();