# PHP SYSTEM

Este repositório contém o projeto System com nível de usuários.<br>
Leia mais no https://www.sysoeducation.com.br<br>

O System é um Sistema de Postagem desenvolvido pelo canal Syso Education, cujo objetivo é gerenciar toda parte de postagens e usuarios com algumas funções internas do próprio sistema.<br>
O Sistema foi desenvolvido na linguagem PHP, utilizando juntamente com as funções do curso Crud com mysqli e System com login e senha. O banco de dados utilizado é o famoso MySQL.
<br>
O desenvolvimento desse sistema tem como objetivo, ensinar aos apaixonados por programação, ensinar uma maneira de desenvolver um sistema para gerenciamento de dados, seja ele para um site instituicional, portal de notícias, etc.<br>
Espero que todos tenha um ótimo aprendizado, que vocês usem os conceitos aprendidos nesse projeto para aplicações em outros.<br>
Forte abraço, e até a próxima!

## Como instalar
1° Acesse o link [https://gitlab.com/syso-education/system-com-nivel-de-usuario](https://gitlab.com/syso-education/system-com-nivel-de-usuario) e baixe o projeto final.<br>
2° Dentro da pasta (_materiais) que se encontra na pasta do projeto, tem o arquivo chamado ( system.sql ) do banco de dados.<br>
Crie um banco com o nome que preferir, nesse caso ( system ), abra o system.sql com o notpad ++ e copie as tabelas, depois cole o codigo no sql e execute.<br>
3° No config/config.php, adcionar a url do seu servidor local no define('SYSTEM', 'https://www.sysoeducation.com.br'; OBS: NÃO DEIXAR A ULTIMA BARRA);<br> 
4° OBS: Esse projeto é com base nas aulas system: Link: https://www.youtube.com/watch?v=8xlnKyolVDk

<hr>

## OBS
1° Caso esteje utilizando o wampserver, você precisa habilitar o short open tag do seu servidor como mostra a imagem abaixo.<br>
https://s7.postimg.org/jo6xrnf3f/short_open_tag.jpg<br>

2° Cao esteje utilizando o wampserver, você precisa habilitar o mod_rewrite do seu servidor local, como mostra a imagem abaixo<br>
https://i.stack.imgur.com/2WvMU.png

## Para logar no sistema utilize

Admin Master: Nível 1 -> master@system.com.br<br>

Admin: Nível 2 -> admin@system.com.br<br>
Moderador: Nível 3 -> moderador@system.com.br<br>

Senha: 123456

## License open source
O SYSTEM é open-source e nossa licença é <a href="https://opensource.org/licenses/MIT" target="_blank">MIT<a/><br>

É concedida permissão a qualquer pessoa que obtenha uma cópia deste software e dos arquivos associados ao ("Software"), para negociar o Software sem restrições, incluindo, sem limitação, os direitos de uso, cópia, modificação, mesclagem , Publicar, distribuir e vender o Software. É permitir que as pessoas a quem o Software é fornecido o façam, sujeito às seguintes condições:<br>
O aviso de copyright acima e este aviso de permissão devem ser incluídos em todas as cópias ou partes substanciais do Software.<br>
O SOFTWARE É FORNECIDO "NO ESTADO EM QUE SE ENCONTRA", SEM GARANTIA DE QUALQUER TIPO, EXPRESSA OU IMPLÍCITA, INCLUINDO, MAS NÃO SE LIMITANDO ÀS GARANTIAS DE COMERCIALIZAÇÃO, ADEQUAÇÃO A UM DETERMINADO PROPÓSITO E NÃO-INFRAÇÃO. EM NENHUMA CIRCUNSTÂNCIA OS AUTORES OU TITULARES DE DIREITOS AUTORAIS SERÃO RESPONSÁVEIS POR QUALQUER RECLAMAÇÃO, DANOS OU OUTRA RESPONSABILIDADE, SEJA EM UMA ACÇÃO DE CONTRATO, ATO ILÍCITO OU DE OUTRA FORMA, DECORRENTE DE, OU EM CONEXÃO COM O SOFTWARE OU O USO OU OUTROS NEGÓCIOS NO PROGRAMAS.

## Contato
Professor: Reinaldo Dorti<br>
E-mail: reinaldorti@sysoeducation.com.br<br>
Portal: https://www.sysoeducation.com.br

Professor: Aldemir Souza<br>
E-mail: contato@sysoeducation.com.br<br>
Portal: https://www.sysoeducation.com.br

## Links das aulas
Este projeto é uma versão do SYSTEM. Para obter mais informações, dê uma olhada nesses vídeos:

01 - https://www.youtube.com/watch?v=pWtxzLxPzrk<br>
02 - https://www.youtube.com/watch?v=8ji7eJKLIkQ<br>
03 - https://www.youtube.com/watch?v=G-z8ZYoSdPI<br>
04 - https://www.youtube.com/watch?v=kqW7FUao6HQ<br>
05 - https://www.youtube.com/watch?v=Oi09TtZNvvc<br>
06 - https://www.youtube.com/watch?v=ww7ItxbG2WY<br>
07 - https://www.youtube.com/watch?v=8HL0lDZ4oxU<br>
08 - https://www.youtube.com/watch?v=zubynjmYXb4<br>
09 - https://www.youtube.com/watch?v=91QWQTMICew<br>
10 - https://www.youtube.com/watch?v=HjFdkOCbuGc<br>
11 - https://www.youtube.com/watch?v=slufIBidYv4<br>
12 - https://www.youtube.com/watch?v=6zWTWit7hTc<br>
13 - https://www.youtube.com/watch?v=IluRgqUJk6M<br>
14 - https://www.youtube.com/watch?v=h-56yWaY250<br>
15 - https://www.youtube.com/watch?v=QbKg4eqgno4<br>
16 - https://www.youtube.com/watch?v=Y42dZJPZl4A<br>
17 - https://www.youtube.com/watch?v=crqY8Bo26qQ<br>
18 - https://www.youtube.com/watch?v=tBCSOMYh9Yk<br>
19 - https://www.youtube.com/watch?v=C0KukMheKAQ<br>

<hr>

## Agradecimento:
> Te desejo muito sucesso e espero que você nunca pare de estudar, pois a tecnologia não para e nós, por mais difícil que<br> seja, devemos acompanhar esta evolução.

#### Um forte abraço do seu amigo [Reinlado Dorti](https://www.sysoeducation.com.br/ "Reinaldo Dorti").