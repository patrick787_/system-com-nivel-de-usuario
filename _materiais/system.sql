CREATE DATABASE  IF NOT EXISTS `system` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `system`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: system
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `data` timestamp NULL DEFAULT NULL,
  `auth` int(11) DEFAULT NULL,
  `data_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Esporte',1,'2017-07-22 10:03:05',1,NULL),(2,'Cidade',1,'2017-07-22 10:03:05',1,NULL),(3,'Economia',1,'2017-07-22 10:03:05',1,NULL),(4,'Politica',1,'2017-07-22 10:03:05',1,NULL);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_userid` int(11) DEFAULT NULL,
  `log_data` timestamp NULL DEFAULT NULL,
  `log_hostname` varchar(255) DEFAULT NULL,
  `log_ip` varchar(255) DEFAULT NULL,
  `log_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manutencao`
--

DROP TABLE IF EXISTS `manutencao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manutencao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `auth` int(11) DEFAULT NULL,
  `data_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manutencao`
--

LOCK TABLES `manutencao` WRITE;
/*!40000 ALTER TABLE `manutencao` DISABLE KEYS */;
INSERT INTO `manutencao` VALUES (1,1,1,'2017-07-28 10:18:01');
/*!40000 ALTER TABLE `manutencao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth` int(11) DEFAULT NULL,
  `titulo` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `texto` text CHARACTER SET latin1,
  `data` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  `categoria` int(11) DEFAULT NULL,
  `thumb` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `data_update` timestamp NULL DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recover`
--

DROP TABLE IF EXISTS `recover`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recover` (
  `cod_id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_userid` int(11) DEFAULT NULL,
  `cod_chave` varchar(255) DEFAULT NULL,
  `cod_data` timestamp NULL DEFAULT NULL,
  `cod_data_end` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recover`
--

LOCK TABLES `recover` WRITE;
/*!40000 ALTER TABLE `recover` DISABLE KEYS */;
/*!40000 ALTER TABLE `recover` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `senha` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'senha de acesso no sistema',
  `code` varchar(100) CHARACTER SET latin1 DEFAULT NULL COMMENT 'recuperacao de senha',
  `nivel` int(11) DEFAULT NULL COMMENT 'nivel de acesso no sistema',
  `status` int(11) DEFAULT NULL COMMENT 'status de acesso no sistema',
  `data` timestamp NULL DEFAULT NULL,
  `auth` int(11) DEFAULT NULL COMMENT 'pega o id de quem está atualizando o usuario',
  `data_update` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'mostra a data e hora da ultima alteração do usuario',
  `log` int(11) DEFAULT '0' COMMENT 'log para indentificar quem ta logado, 0 deslogado e 1 logado',
  `log_in` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'mostra a data e hora de quando o usuario logo',
  `log_in_time` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'atualiza a data e hora para ver quanto tempo ele fica na pagina',
  `log_out` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'mostra a data e hora de quando o usuario deslogo do sistema',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Master','master@system.com.br','ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413','123456',1,1,'2016-11-09 12:37:07',1,'2018-01-03 13:07:23',1,'2018-01-16 16:11:08','2018-01-16 17:11:53','2018-01-16 14:02:57'),(2,'Admin','admin@system.com.br','ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413','123456',2,1,'2017-01-01 12:08:22',1,'2018-01-16 12:17:50',0,'2018-01-16 11:59:56','2018-01-16 13:17:26','2018-01-16 12:17:26'),(4,'Moderador','moderador@system.com.br','ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413','123456',3,1,'2017-02-10 20:37:43',1,'2018-01-16 12:20:31',0,'2018-01-16 13:48:50','2018-01-16 15:00:47','2018-01-16 14:00:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `data` timestamp NULL DEFAULT NULL,
  `data_update` timestamp NULL DEFAULT NULL,
  `auth` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (1,'Charlie Brown Jr - Música Popular Caiçara (DVD Oficial) ','t7Ws56idM_s',1,'2017-07-28 10:11:34',NULL,1),(2,'Charlie Brown JR - Ao vivo no Planeta Atlântida 2012 ','5xGUIbmoJiA',1,'2017-07-28 10:12:07',NULL,1),(3,'2hr7Uqu6G80','2hr7Uqu6G80',1,'2017-07-28 10:12:56',NULL,1),(4,'Legião Urbana - Faroeste Caboclo','eL6zdEwRKws',1,'2017-07-28 10:13:20',NULL,1),(5,'Legião Urbana - Índios','nM_gEzvhsM0',1,'2017-07-28 10:14:01','2017-07-28 10:14:15',1);
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-16 14:32:30
