<?php
ob_start();
session_start();
require_once('config/crud.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <script src="<?= SYSTEM; ?>/js/jquery.js"></script>
        <link href="<?= SYSTEM; ?>/css/style.css" rel="stylesheet" type="text/css"/>        
    </head>
    <body>
        <div class="box">
            <?php
            //RECUPERA OS DADOS NO BANCO
            $read = read('videos', "WHERE status = 1 ORDER BY RAND() LIMIT 1");
            if (!$read):
                echo '<span class="ms no">Oppss! Não existe vídeos cadastrados no momento. Por favor, volte mais tarde!</span>';
            else:
                //foreach ($read as $res); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                $res = mysqli_fetch_array($read);
                echo '<div class="ratiohd">';
                echo '<iframe class="target ratio_el" width="100%" src="https://www.youtube.com/embed/' . $res['link'] . '?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>';
                echo '</div>';
            endif;

            echo '<hr>';

            //RECUPERA OS DADOS NO BANCO
            $readThumb = read('videos', "WHERE status = 1");
            if (!$readThumb):
                echo '';
            else:
                foreach ($readThumb as $row):
                    echo '<img class="clicar" src="http://img.youtube.com/vi/' . $row['link'] . '/default.jpg" style="border: 0px; cursor: pointer;" rel="https://www.youtube.com/embed/' . $row['link'] . '?rel=0&autoplay=1" title="' . $row['titulo'] . '" />';
                endforeach;
            endif;
            ?>
        </div>                
    </body>
    <script type="text/javascript">
        $(function () {
            //$(".clicar").click(function () { ATUALIZADO PARA ON CLICK
            $(".clicar").on('click', function () {
                var action = $(this).attr("rel");
                $(".target").attr("src", action);
            });
        });
    </script>
</html>
<?php
ob_end_flush();
