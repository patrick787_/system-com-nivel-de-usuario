<?php
//VERIFICA SE EXISTE UMA SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;