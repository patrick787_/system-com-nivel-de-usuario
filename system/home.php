<div class="span12">
    <div id="target-1" class="widget">
        <div class="widget-content" style="padding: 20px 15px 15px;">
            <h1>System - Com nível de usuário</h1>
            <p>O <strong>System</strong> é um Sistema de Postagem desenvolvido pelo canal '<strong>
                    Syso Education</strong>', cujo objetivo é gerenciar toda parte de postagens e
                algumas funções internas do próprio sistema.<br>
                O Sistema foi desenvolvido na linguagem <strong>PHP</strong>, utilizando juntamente com as funções do
                curso crud com mysqli e system com login e senha. O banco de dados utilizado é o famoso <strong>MySQL</strong>.
            </p>

            <p>O desenvolvimento desse Sistema tem como objetivo, ensinar aos apaixonados a <strong>Programar</strong>, ensinar uma maneira de desenvolver um sistema para gerenciamento de dados, seja ele para um site instituicional, portal de notícias, etc.
            </p>

            <p>Espero que todos tenha um ótimo aprendizado, que vocês usem os conceitos aprendidos nesse projeto para aplicações em outros.</p>
            <p><strong>Forte abraço, e até a próxima!</strong></p>
        </div>
    </div>
</div>