<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>;
?>
<div class="widget widget-table action-table">
    <div class="widget-header"><i class="icon-th-list"></i>
        <h3>Listar</h3>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>                  
                    <th>Data de cadastro</th>
                    <th>Status</th>
                    <th class="td-actions" style="width: 120px;">Ações</th>
                </tr>
            </thead>
            <?php
            //ativar e desatiar o usuario
            if(!empty($_GET['id'])):
                $StatuId = $_GET['id'];
                $acao = $_GET['acao'];

                switch ($acao):
                    case 'ativar':
                        $acao = array('status'=>'1','auth'=>$userId,'data_update'=>date("Y-m-d H:i:s"));
                        update(TAB_VIDEOS,$acao, "id = '$StatuId' ");
                        echo '<span class="ms ok">Pronto! Status foi ativado com sucesso!</span>';
                        echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHVIDEOS . '/videos-home">';
                        break;
                    case 'bloquear':
                        $acao = array('status'=>'2','auth'=>$userId,'data_update'=>date("Y-m-d H:i:s"));
                        update(TAB_VIDEOS,$acao, "id = '$StatuId' ");
                        echo '<span class="ms ok">Pronto! Status foi bloqueado com sucesso!</span>';
                        echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHVIDEOS . '/videos-home">';
                        break;
                    default:
                        echo '<span class="ms no">Oppss! Ação não indentificado pelo sistema. Por favor, tente novamente!</span>';
                        echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHVIDEOS . '/videos-home">';
                endswitch;
            endif;

            //deleta usuarios
            if (!empty($_GET['delete'])):
                $delId = $_GET['delete'];

                //deleta a videos através do id
                delete(TAB_VIDEOS, "id = '$delId'");
                echo C_DELETADO;
                echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHVIDEOS . '/videos-home">';
            endif;

            //leitura da tabela users
            $readUser = read(TAB_VIDEOS, "ORDER BY id DESC");
            if (!$readUser):
                echo '<span class="ms no">Oppss! Categoria não existe! Por favor, tente mais tarde!</span>';
                echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHVIDEOS . '/videos-create">';
            else:
                foreach ($readUser as $rows):
                    $ico = ($rows['status'] == 1 ? 'active' : 'inactive');
                    //$status = ($rows['status'] == 1 ? 'Ativo' : 'Bloqueado');
                    ?>
                    <tr>
                        <td><?= $rows['titulo']; ?></td>
                        <td><?= date('d/m/Y H:i:s', strtotime($rows['data'])); ?></td>
                        <td class="<?= $ico; ?>"><?= FuncStatus($rows['status']); ?></td>
                        <td class="td-actions">
                            <a href="<?= R_REFRESHVIDEOS; ?>/videos-update&id=<?= $rows['id']; ?>" title="Editar" class="btn btn-small btn-success">
                                <i class="btn-icon-only icon-edit"></i>
                            </a>
                            <a href="<?= R_REFRESHVIDEOS; ?>/videos-home&delete=<?= $rows['id']; ?>" title="Deletar" class="btn btn-danger btn-small">
                                <i class="btn-icon-only icon-remove"></i>
                            </a>
                            <?php
                            if($rows['status'] == 1):
                                echo'<a href="'. R_REFRESHVIDEOS .'/videos-home&id='. $rows['id'] .'&acao=bloquear" title="Bloquear" class="btn btn-small">
                                        <i class="btn-icon-only icon-eye-open"></i>
                                    </a>';
                            else:
                                echo'<a href="'. R_REFRESHVIDEOS .'/videos-home&id='. $rows['id'] .'&acao=ativar" title="Ativar" class="btn btn-small">
                                        <i class="btn-icon-only icon-eye-close"></i>
                                    </a>';
                            endif;
                            ?>
                        </td>
                    </tr>
                    <?php
                endforeach;
            endif;
            ?>
        </table>
    </div>
</div>
