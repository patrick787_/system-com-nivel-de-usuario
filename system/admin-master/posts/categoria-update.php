<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="target-1" class="widget">
                        <div class="widget-header">
                            <i class="icon-folder-close"> Atualizar</i>
                        </div>
                        <div class="widget-content">
                            <div class="tab-pane">
                                <?php
                                $catId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (!empty($data['enviar'])):
                                    unset($data['enviar']);

                                    //valida os campos
                                    if (in_array('', $data)):
                                        echo C_BRANCOS;
                                    else:
                                        //armazena os dados para gravar no banco
                                        $data['auth'] = $_SESSION['autUser']['id'];
                                        $data['data_update'] = date('Y-m-d H:i:s');

                                        //update no banco
                                        update(TAB_CATEGORIA, $data, "id='$catId'");
                                        echo C_SALVO;
                                        echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/categoria-home">';
                                    endif;
                                endif;

                                //faz uma leitura na tabela categoria através do id
                                $readCat = read(TAB_CATEGORIA, "WHERE id='$catId'");
                                if (!$readCat):
                                    echo '<span class="ms no">Oppss! Essa categoria não existe, tente novamente!</span>';
                                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/categoria-home">';
                                else:
                                    foreach ($readCat as $data):
                                        ?>
                                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                            <div class="control-group">
                                                <div style="clear: both;"></div>
                                                <label class="control-label">Nome:</label>
                                                <div class="controls">
                                                    <input type="text" class="span6 disabled" name="nome" value="<?php if (isset($data['nome'])) echo $data['nome']; ?>">
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Status</label>
                                                <div class="controls">
                                                    <select class="span2" name="status">
                                                        <option value="">Selecione...</option>
                                                        <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                        <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-actions"
                                                 style="background: none !important; border: none;">
                                                <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">
                                            </div>
                                        </form>
                                        <?php
                                    endforeach;
                                endif
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
