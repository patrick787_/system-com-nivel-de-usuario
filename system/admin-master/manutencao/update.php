<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="target-1" class="widget">
                        <div class="widget-header">
                            <i class="icon-user"> Manuten&ccedil;&atilde;o</i>
                        </div>
                        <div class="widget-content">
                            <div class="tab-pane">
                                <?php
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (isset($_POST['enviar'])):
                                    unset($data['enviar']);

                                    //armazena os dados para gravar no banco
                                    $data['auth'] = $_SESSION['autUser']['id'];
                                    $data['data_update'] = date('Y-m-d H:i:s');

                                    //faz o update no banco atravez do id
                                    update(TAB_MANUTENCAO, $data, "id='$data[id]'");
                                    echo C_SALVO;
                                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHMANUTENCAO . '/update">';
                                endif;

                                //faz uma leitura no banco usando a funcao read
                                $readUser = read(TAB_MANUTENCAO);
                                if (!$readUser):
                                    echo '<span class="ms no">Oppss! Não existe manutenção no momento!</span>';
                                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHHOME . '">';
                                else:
                                    foreach ($readUser as $data):
                                        ?>
                                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                            <div class="control-group">
                                                <label class="control-label">Status</label>
                                                <div class="controls">
                                                    <select class="span2" name="status">
                                                        <option value="">Selecione...</option>
                                                        <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                        <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <input type="hidden" name="id" value="<?= $data['id'] ?>"/>
                                            <div class="form-actions" style="background: none !important; border: none;">
                                                <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">
                                            </div>
                                        </form>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
