<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="widget widget-table action-table">
    <div class="widget-header"><i class="icon-th-list"></i>
        <h3>Listar usu&aacute;rios</h3>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Nível</th>
                    <th>Status</th>
                    <th class="td-actions" style="width: 120px;">Ações</th>
                </tr>
            </thead>
                <?php
                //ativa de desativa usuarios
                if(!empty($_GET['id'])):
                    $StatuId = $_GET['id'];
                    $acao = $_GET['acao'];

                    //bloqueia o usuario se for o mesmo da sessão
                    if($StatuId == $userId):
                        echo '<span class="ms no">Oppss! Essa ação não é permitida para esse usuario. Por favor, tente novamente!</span>';
                    else:
                        switch ($acao):
                            case 'ativar':
                                $acao = array('status'=>'1','auth'=>$userId,'data_update'=>date("Y-m-d H:i:s"));
                                update(TAB_USERS,$acao, "id = '$StatuId' ");
                                echo '<span class="ms ok">Pronto! Status foi ativado com sucesso!</span>';
                                //echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users-home">';
                            break;
                            case 'bloquear':
                                $acao = array('status'=>'2','auth'=>$userId,'data_update'=>date("Y-m-d H:i:s"));
                                update(TAB_USERS,$acao, "id = '$StatuId' ");
                                echo '<span class="ms ok">Pronto! Status foi bloqueado com sucesso!</span>';
                                //echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users-home">';
                            break;
                            default:
                            echo '<span class="ms no">Oppss! Ação não indentificado pelo sistema. Por favor, tente novamente!</span>';
                            echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users-home">';
                        endswitch;
                    endif;
                endif;

                //deleta usuarios
                if (!empty($_GET['delete'])):
                    $delUserId = $_GET['delete'];
                    $userId = $_SESSION['autUser']['id'];

                    //valida o usuario para não deletar o perfil
                    if ($delUserId == $userId):
                        echo '<span class="ms no">Oppss! Você não pode deletar seu perfil!</span>';
                        echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users-home">';
                    else:
                        //deleta o usuario através do id
                        delete(TAB_USERS, "id = '$delUserId'");
                        echo C_DELETADO;
                        echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users-home">';
                    endif;
                endif;

                //leitura da tabela users do banco de dados
                $readUser = read(TAB_USERS, "ORDER BY id DESC");
                if (!$readUser):
                    echo '<span class="ms no">Oppss! Não existes usuários cadastrados no momento!</span>';
                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHHOME . '/users-home">';
                else:
                    foreach ($readUser as $rows):
                        $ico = ($rows['status'] == 1 ? 'active' : 'inactive');
                        //$status = ($rows['status'] == 1 ? 'Ativo' : 'Bloqueado');
                        //$nivel = ($rows['nivel'] == 1 ? 'Admin Master' : ($rows['nivel'] == 2 ? 'Admin' : 'Moderador'));
                        ?>
                        <tr>
                            <td><?= $rows['nome']; ?></td>
                            <td><?= $rows['email']; ?></td>
                            <td><?= FuncNivel($rows['nivel']); ?></td>
                            <td class="<?= $ico; ?>"><?= FuncStatus($rows['status']); ?></td>

                            <?php 
                            //NÃO DIEXA O USUARIO EXCLUR SEU PROPRIO PERFIL ( $user['id'] vem da index )
                            if($rows['nivel'] == 1 && $rows['id'] != $user['id']):
                                echo'<td class="td-actions"></td>';
                            else:
                            ?>
                                <td class="td-actions">
                                    <a href="<?= R_REFRESHUSERS; ?>/users-update&id=<?= $rows['id']; ?>" title="Editar" class="btn btn-small btn-success">
                                        <i class="btn-icon-only icon-edit"></i>
                                    </a>
                                    <a href="<?= R_REFRESHUSERS; ?>/users-home&delete=<?= $rows['id']; ?>" title="Deletar" class="btn btn-danger btn-small">
                                        <i class="btn-icon-only icon-remove"></i>
                                    </a>
                                    <?php
                                    if($rows['status'] == 1):
                                        echo'<a href="'. R_REFRESHUSERS .'/users-home&id='. $rows['id'] .'&acao=bloquear" title="Bloquear" class="btn btn-small">
                                            <i class="btn-icon-only icon-eye-open"></i>
                                        </a>';
                                    else:
                                        echo'<a href="'. R_REFRESHUSERS .'/users-home&id='. $rows['id'] .'&acao=ativar" title="Ativar" class="btn btn-small">
                                            <i class="btn-icon-only icon-eye-close"></i>
                                        </a>';
                                    endif;
                                    ?>
                                </td>
                            <?php  endif; ?>
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>
        </table>
    </div>
</div>
