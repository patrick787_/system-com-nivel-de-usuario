<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="widget widget-table action-table">
    <div class="widget-header"><i class="icon-filter"></i>
        <h3>Pesquisa</h3>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Nível</th>
                    <th>Status</th>
                    <th class="td-actions" style="width: 110px;">Ações</th>
                </tr>
            </thead>
            <?php
            //pesquisar
            $get = filter_input(INPUT_GET, 's', FILTER_DEFAULT);
            $search = urldecode($get);
            $search = "and nome LIKE '%$search%' OR email LIKE '%$search%'";

            //deleta usuarios
            if (!empty($_GET['delete'])):
                $delUserId = $_GET['delete'];
                $userId = $_SESSION['autUser']['id'];

                //valida o id e não deixa deletar o perfil
                if ($delUserId == $userId):
                    echo '<span class="ms no">Oppss! Você não pode deletar seu perfil!</span>';
                else:
                    delete(TAB_USERS, "id = '$delUserId'");
                    echo C_DELETADO;
                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users">';
                endif;
            endif;

            //leitura da tabela users
            $readUser = read(USERS, "WHERE id !='' {$search}");
            if (!$readUser):
                echo '<span class="ms no">Oppss! Nada encontrado, tente novamente!</span>';
                echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users">';
            else:
                foreach ($readUser as $rows):
                    $ico = ($rows['status'] == 1 ? 'active' : 'inactive');
                    //$status = ($rows['status'] == 1 ? 'Ativo' : 'Bloqueado');
                    //$nivel = ($rows['nivel'] == 1 ? 'Admin' : 'Moderador');
                    ?>
                    <tr>
                        <td><?= $rows['nome']; ?></td>
                        <td><?= $rows['email']; ?></td>
                        <td><?= FuncNivel($rows['nivel']); ?></td>
                        <td class="<?= $ico; ?>"><?= FuncStatus($rows['status']); ?></td>
                        <?php 
                        if($rows['nivel'] == 1):
                            echo'<td class="td-actions"></td>';
                        else:
                            ?>
                            <td class="td-actions">
                                <a href="<?= R_REFRESHUSERS; ?>/users-update&id=<?= $rows['id']; ?>" title="Editar" class="btn btn-small btn-success">
                                    <i class="btn-icon-only icon-edit"></i>
                                </a>
                                <a href="<?= R_REFRESHUSERS; ?>/users-home&delete=<?= $rows['id']; ?>" title="Deletar" class="btn btn-danger btn-small">
                                    <i class="btn-icon-only icon-remove"></i>
                                </a>
                                <?php
                                if($rows['status'] == 1):
                                    echo'<a href="'. R_REFRESHUSERS .'/users-home&id='. $rows['id'] .'&acao=bloquear" title="Bloquear" class="btn btn-small">
                                        <i class="btn-icon-only icon-eye-open"></i>
                                    </a>';
                                else:
                                    echo'<a href="'. R_REFRESHUSERS .'/users-home&id='. $rows['id'] .'&acao=ativar" title="Ativar" class="btn btn-small">
                                        <i class="btn-icon-only icon-eye-close"></i>
                                    </a>';
                                endif;
                                ?>
                            </td>
                        <?php  endif; ?>
                    </tr>
                    <?php
                endforeach;
            endif;
            ?>
        </table>
    </div>
</div>