<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="target-1" class="widget">
                        <div class="widget-header">
                            <i class="icon-user"> Editar</i>
                        </div>
                        <div class="widget-content">
                            <div class="tab-pane">
                                <?php
                                $userId = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (isset($_POST['enviar'])):
                                    unset($data['enviar']);

                                    //valida os campos
                                    if (empty($data['nome'])):
                                        echo '<span class="ms no">Oppss! Nome está em branco!</span>';
                                    elseif (empty($data['email'])):
                                        echo '<span class="ms no">Oppss! E-mail está em branco!</span>';
                                    elseif (!ValidarEmail($data['email'])):
                                        echo '<span class="ms no">Oppss! E-mail inválido! Por favor, digite um e-mail valido!</span>';
                                    elseif (strlen($_POST['senha']) > 0 and ( strlen($_POST['senha']) < 6 || strlen($_POST['senha']) > 15)):
                                        echo '<span class="ms no">Oppss! Senha deve conter de 6 á 15 caracteres. Por favor, tente novamente para continuar!</span>';
                                    elseif ($data['senha'] != $data['code']):
                                        echo '<span class="ms no">Oppss! Por favor, repita a senha igual o campo senha!</span>';
                                    else:
                                        //verifica se o email existe no banco de dados e nao deixa cadastrar
                                        $readEmail = read(TAB_USERS, "WHERE email = '$data[email]' AND id != '$userId' ");
                                        if (mysqli_num_rows($readEmail) >= 1):
                                            echo '<span class="ms no">Oppss! E-mail já existe no sistema. Por favor, cadastre um e-mail iferente!</span>';
                                        else:
                                            //armazena os dados oculdo que o usuario nao cadastro para atualizar no banco
                                            $data['auth'] = $_SESSION['autUser']['id'];
                                            $data['data_update'] = date("Y-m-d H:i:s");
                                            $data['code'] = $data['senha'];
                                            //$data['senha'] = md5($data['code']);
                                            $data['senha'] = hash('sha512', $data['code']); //ALTERADO PARA shar512
                                                                                                                                    
                                            //verifica se a senha esta em branco para nao atualizar no banco
                                            if (empty($data['code'])):
                                                unset($data['senha']);
                                                unset($data['code']);
                                            endif;  

                                            //usa a funcao para atualizar a tabela users
                                            update(TAB_USERS, $data, "id = '$userId'");
                                            echo C_EDITADO;
                                            echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users-home">';
                                        endif;
                                    endif;
                                endif;

                                //faz uma leitura usando a funcao read
                                $readUser = read(TAB_USERS, "WHERE id='$userId'");
                                if (mysqli_num_rows($readUser) < 1):
                                    echo '<span class="ms no">Oppss! Não existe esse usuario, tente novamente!</span>';
                                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHUSERS . '/users-home">';
                                else:
                                    foreach ($readUser as $data):

                                        //verifica se o nivel é do user logado e direciona para o perfil                                        
                                        if ($data['nivel'] == 1 && $userId == $data['id']):
                                            header('Location: ' . R_REFRESHUSERS . '/users-perfil');
                                        else:
                                            ?>
                                            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                                <div class="control-group">
                                                    <div style="clear: both;"></div>
                                                    <label class="control-label">Nome:</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6 disabled" name="nome" value="<?php if (isset($data['nome'])) echo $data['nome']; ?>"/>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">E-mail:</label>
                                                    <div class="controls">
                                                        <input type="text" class="span6 disabled" name="email" value="<?php if (isset($data['email'])) echo $data['email']; ?>">
                                                    </div>
                                                </div> 

                                                <div class="control-group">
                                                    <label class="control-label">Senha:</label>
                                                    <div class="controls">
                                                        <input type="password" class="span6 disabled" name="senha">
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Repita a senha:</label>
                                                    <div class="controls">
                                                        <input type="password" class="span6 disabled" name="code">
                                                        <p id="resultado" style="color: #ba4a48; font-weight: bold; float: right; margin-right: 180px; font-size: 11px; width: auto; margin-top: 5px; text-align: right;"></p>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Status</label>
                                                    <div class="controls">
                                                        <select class="span2" name="status">
                                                            <option value="">Selecione...</option>
                                                            <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                            <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">N&iacute;vel</label>
                                                    <div class="controls">
                                                        <select class="span2" id="exibir" name="nivel">
                                                            <option value="">Selecione...</option>
                                                            <option value="2" <?php if (isset($data['nivel']) && $data['nivel'] == 2) echo 'selected="selected"'; ?>>Admin</option>
                                                            <option value="3" <?php if (isset($data['nivel']) && $data['nivel'] == 3) echo 'selected="selected"'; ?>>Moderador</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-actions"
                                                     style="background: none !important; border: none;">
                                                    <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">
                                                </div>
                                            </form>
                                        <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>