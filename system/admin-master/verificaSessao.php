<?php
//verifica se existe uma sessão do usuário
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;