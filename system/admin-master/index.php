<?php
ob_start();
session_start();
require_once('../../config/crud.php');
require_once('../../config/funcoes.php');

//pega os dados do usuario atraves do id da sessão
$userId = $_SESSION['autUser']['id'];

//funcao que captura o log do usuario
SaveLog();

//atualiza o campo log_in_time do usuario
starTime();

//verifica se a sessão não está vazia
if (empty($_SESSION['autUser'])):
    header('Location:' . SYSTEM . '/index.php?exe=restrito');
    die;
endif;

//faz a leitura extraindo os dados do usuario
$readUser = read(TAB_USERS, "WHERE id = '$userId'");
//foreach ($readCat as $catname); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
$user = mysqli_fetch_array($readUser);

//verifica se o usuario está ativo no sistema
if ($user['status'] != '1'):
    unset($_SESSION['autUser']);
    header('Location:' . SYSTEM . '/index.php?exe=permissao');
    die;
endif;

//verifica se o usuario esta logado no sistema
if ($user['log'] != '1'):
    unset($_SESSION['autUser']);
    header('Location:' . SYSTEM . '/index.php?exe=permissao');
    die;
endif;

//verifica se o nivel do usuario tem permissao para acessar esta area
if ($user['nivel'] != '1'):
    logar();
endif;

//faz o logoff do sistema
$sair = filter_input(INPUT_GET, 'sair', FILTER_VALIDATE_BOOLEAN);
if ($sair):
    //reseta o campo log e coloca a data no logout
    $logout = array('log' => '0', 'log_out' => date("Y-m-d H:i:s"));
    update(TAB_USERS, $logout, "id = $user[id]");

    //mata a sessão e desloga o usuario
    unset($_SESSION['autUser']);
    header('Location:' . SYSTEM . '/system/index.php?exe=sair');
    die;
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Sistema de Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/pages/dashboard.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
        require_once('topo.php');
        require_once('menu.php');
        ?>
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="row">
                        <?php
                        //if (empty($_GET['exe'])):
                        //require_once('../home.php');
                        //elseif (file_exists($_GET['exe'] . '.php')):
                        //require_once($_GET['exe'] . '.php');
                        //else:
                        //require_once('../error.php');
                        //endif;
                        
                        //ATUALIZADO PARA OS FILTROS DO PHP						
                        $getexe = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);
                        if (empty($getexe)):
                            require('../home.php');
                        elseif (file_exists($getexe . '.php')):
                            require_once($getexe . '.php');
                        else:
                            require_once('../error.php');
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        require('../footer.php');
        ?>
        <!-- MicEdit -->       
        <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
        <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>						

        <script src="../js/jquery-1.7.2.min.js"></script>
        <script src="../js/excanvas.min.js"></script>
        <script src="../js/chart.min.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/full-calendar/fullcalendar.min.js"></script>
        <script src="../js/base.js"></script>
    </body>
</html>
<?php
ob_end_flush();
