<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">
                <li>                    
                    <a href="index.php" title="Home">
                        <i class="icon-dashboard"></i>
                        <span>Home</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-file"></i>
                        <span>Postagens</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= R_REFRESHPOSTS; ?>/post-create" title="Cadastrar">Cadastrar</a></li>
                        <li><a href="<?= R_REFRESHPOSTS; ?>/post-home" title="Visualizar">Visualizar</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-facetime-video"></i>
                        <span>Vídeos</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= R_REFRESHVIDEOS; ?>/videos-create" title="Cadastrar ">Cadastrar</a></li>
                        <li><a href="<?= R_REFRESHVIDEOS; ?>/videos-home" title="Visualizar vídeos">Visualizar</a></li>
                    </ul>
                </li>
                <li></li>
            </ul>
        </div>
    </div>
</div>
