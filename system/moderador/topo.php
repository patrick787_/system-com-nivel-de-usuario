<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </a>
            <a class="brand">System</a>
            <div class="nav-collapse">
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-cog"></i> Op&ccedil;&otilde;es <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <?php
                            $readManu = read(TAB_MANUTENCAO);
                            foreach($readManu as $re);
                            $ico = ($re['status'] == 1 ? 'active' : 'inactive');
                            ?>
                            <li><a href="../../" target="_blank"><span class="<?= $ico; ?>">Visitar Site</span></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i> <?= $user['nome']; ?><b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= R_REFRESHUSERS; ?>/users-perfil">Perfil</a></li>
                            <li><a href="<?= R_REFRESHHOME; ?>?sair=true">Sair</a></li>
                        </ul>
                    </li>
                </ul>
                <?php
                //pesquisa no sistema
                $search = filter_input_array(INPUT_POST);
                if ($search && $search['s']):
                    $s = urlencode($search['s']);
                    header("Location: index.php?exe=users/search&s={$s}");
                endif;
                ?>
                <form name="form" action="" method="post" enctype="multipart/form-data" class="navbar-search pull-right">
                    <input type="search" name="s" class="search-query" placeholder="pesquisar">
                </form>
            </div>
        </div>
    </div>
</div>
