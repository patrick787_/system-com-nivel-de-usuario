<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">	      		
                    <div id="target-1" class="widget">	 
                        <div class="widget-header">
                            <i class="icon-facetime-video"> Cadastrar</i>
                        </div>
                        <div class="widget-content">	      				                            
                            <div class="tab-pane">
                                <?php
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (!empty($data['enviar'])):
                                    unset($data['enviar']);

                                    //valida os campos
                                    if (in_array('', $data)):
                                        echo C_BRANCOS;
                                    else:
                                        $readVideo = read(TAB_VIDEOS, "WHERE link='$data[link]'");
                                        if (mysqli_num_rows($readVideo) >= 1):
                                            echo '<span class="ms no">Oppss! Video já existe! Por favor, digite outra categoria!</span>';
                                        else:
                                            //armazena os dados para gravar no banco
                                            $data['auth'] = $_SESSION['autUser']['id'];
                                            $data['data'] = date('Y-m-d H:i:s');

                                            //grava no banco de dados
                                            create(TAB_VIDEOS, $data);
                                            echo C_SALVO;
                                            unset($data);
                                        endif;
                                    endif;
                                endif;
                                ?>
                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                    <div class="control-group">	
                                        <div style="clear: both;"></div>
                                        <label class="control-label">T&iacute;tulo:</label>
                                        <div class="controls">
                                            <input type="text" class="span6 disabled" name="titulo" value="<?php if (isset($data['titulo'])) echo $data['titulo']; ?>">
                                        </div>			
                                    </div>

                                    <div class="control-group">	
                                        <div style="clear: both;"></div>
                                        <label class="control-label" for="username">Link:</label>
                                        <div class="controls">
                                            <input type="text" class="span6 disabled" name="link" value="<?php if (isset($data['link'])) echo $data['link']; ?>">
                                        </div>			
                                    </div>

                                    <div class="control-group">											
                                        <label class="control-label">Status</label>
                                        <div class="controls">
                                            <select class="span2" name="status">
                                                <option value="">Selecione...</option>                           
                                                <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                            </select>
                                        </div>				
                                    </div>                                       

                                    <div class="form-actions" style="background: none !important; border: none;">
                                        <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">                                                                                  
                                    </div>
                                </form>
                            </div>                         
                        </div>
                    </div>
                </div>        
            </div>
        </div>
    </div>
</div>
