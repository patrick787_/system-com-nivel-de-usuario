<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="target-1" class="widget">
                        <div class="widget-header">
                            <i class="icon-file"> Editar</i>
                        </div>
                        <div class="widget-content">
                            <div class="tab-pane">
                                <?php
                                $postid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (!empty($data['enviar'])):
                                    unset($data['enviar']);

                                    $arq = $_FILES['thumb'];

                                    //valida os campos
                                    if (in_array('', $data)):
                                        echo C_BRANCOS;
                                    else:
                                        //leitura do banco de dados para pegar a imagem
                                        $readEdit = read(TAB_POSTS, "WHERE id = '$postid'");                                        
                                        //foreach ($readEdit as $postedit); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                                        $postedit = mysqli_fetch_array($readEdit);
                                        
                                        if (!empty($_FILES['thumb']['tmp_name'])):
                                            $pasta = '../../uploads/';

                                            //remove a imagem da pasta
                                            if (file_exists($pasta . $postedit['thumb']) && !is_dir($pasta . $postedit['thumb'])):
                                                unlink($pasta . $postedit['thumb']);
                                            endif;

                                            //$data['thumb'] = $_FILES['arq'];
                                            $ext = substr($arq['name'], -4);
                                            $data['thumb'] = md5(time()) . $ext;

                                            //move a imagem para a pasta uploads
                                            if (move_uploaded_file($arq['tmp_name'], $pasta . '/' . $data['thumb'])):
                                            endif;
                                        endif;

                                        //update no banco de dados
                                        update(TAB_POSTS, $data, "id = '$postid'");
                                        echo C_SALVO;
                                        echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/post-home">';
                                    endif;
                                endif;

                                //leitura do post no banco de dados
                                $readPost = read(TAB_POSTS, "WHERE id = '$postid' ");
                                if (!$readPost):
                                    echo '<span class="ms no">Oppss! Esse post não existe! Por favor, tente novamente!</span>';
                                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/post-home">';
                                else:
                                    foreach ($readPost as $data):
                                        ?>

                                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                            <div class="control-group">
                                                <div style="clear: both;"></div>
                                                <label class="control-label">Imagem:</label>
                                                <div class="controls">
                                                    <input type="file" class="span6 disabled" name="thumb" title="Imagem" value="<?= $data['thumb']; ?>"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <div style="clear: both;"></div>
                                                <label class="control-label">T&iacute;tulo:</label>
                                                <div class="controls">
                                                    <input type="text" class="span6 disabled" name="titulo" value="<?php if (isset($data['titulo'])) echo $data['titulo']; ?>">
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Categoria</label>
                                                <div class="controls">
                                                    <select class="span2" name="categoria">
                                                        <?php
                                                        //verifica o id da categoria e deixa seleciona no campo
                                                        $readCat = read('categoria', "WHERE id = '$data[categoria]' ");
                                                        if (!mysqli_num_rows($readCat)):
                                                            echo'<option value="">Selecionar</option>';
                                                        else:
                                                            foreach ($readCat as $cate);
                                                            echo'<option value=' . $cate[id] . '>' . $cate[nome] . '</option>';
                                                        endif;

                                                        //leitura da categoria na tabela categoria
                                                        $read = read('categoria', "ORDER BY id DESC");
                                                        if (!$read):
                                                            echo '<span class="ms no">Oppss! Não existe categoria cadastrada no momento! Por favor, mais tarde!</span>';
                                                        else:
                                                            foreach ($read as $cat):
                                                                echo'<option value=' . $cat[id] . '>' . $cat[nome] . '</option>';
                                                            endforeach;
                                                        endif;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Status</label>
                                                <div class="controls">
                                                    <select class="span2" id="exibir"  name="status">
                                                        <option value="">Selecione...</option>
                                                        <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                        <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Texto</label>
                                                <div class="controls">
                                                    <textarea class="span8" name="texto" id="descricao" value="" rows="10"><?php if (isset($data['texto'])) echo $data['texto']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-actions" style="background: none !important; border: none;">
                                                <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">
                                            </div> 
                                        </form>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
