<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">	      		
                    <div id="target-1" class="widget">	 
                        <div class="widget-header">
                            <i class="icon-file"> Cadastrar</i>
                        </div>
                        <div class="widget-content">	      				                            
                            <div class="tab-pane">
                                <?php
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (!empty($data['enviar'])):
                                    unset($data['enviar']);

                                    $arq = $_FILES['arq'];

                                    //valida os campos
                                    if (in_array('', $data)):
                                        echo C_BRANCOS;
                                    else:
                                        $permissao = array('image/jpg', 'image/jpeg', 'image/pjpeg', 'image/png');
                                        $ext = ($arq['type'] == 'image/png' ? '.png' : '.jpg');

                                        //valida a imagem para nao cadastrar outro tipo de arquivo
                                        if (!in_array($arq['type'], $permissao)):
                                            echo '<span class="ms no">Oppss! Por favor, apenas imagem (JPG OU PNG)!</span>';
                                        else:
                                            $data['thumb'] = md5(uniqid()) . $ext;

                                            //armazena o diretorio
                                            $dir = "../../uploads";

                                            //cria diretorio se não existir
                                            if (!checkDir($dir)):
                                                mkdir($dir, 0777);
                                            endif;

                                            //grava a imagem na pasta
                                            if (move_uploaded_file($arq['tmp_name'], $dir . '/' . $data['thumb'])):

                                                //armazena os dados para gravar no banco
                                                $data['auth'] = $_SESSION['autUser']['id'];
                                                $data['data'] = date("Y-m-d H:i:s");

                                                //cadastra no banco de dados
                                                create(TAB_POSTS, $data);
                                                echo C_SALVO;
                                                unset($data);
                                            else:
                                                echo '<span class="ms no">Oppss! Não foi possivel salvar o post! Por favor, tente novamente!</span>';
                                            endif;
                                        endif;
                                    endif;
                                endif;
                                ?>
                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                    <div class="control-group">	
                                        <div style="clear: both;"></div>
                                        <label class="control-label">Imagem:</label>
                                        <div class="controls">                                          
                                            <input type="file" class="span6 disabled" name="arq" title="Imagem" value="<?= $data['arq']; ?>"/>
                                        </div>			
                                    </div>

                                    <div class="control-group">	
                                        <div style="clear: both;"></div>
                                        <label class="control-label">T&iacute;tulo:</label>
                                        <div class="controls">
                                            <input type="text" class="span6 disabled" name="titulo" value="<?php if (isset($data['titulo'])) echo $data['titulo']; ?>">
                                        </div>			
                                    </div>

                                    <div class="control-group">											
                                        <label class="control-label">Categoria</label>
                                        <div class="controls">
                                            <select class="span2" name="categoria">
                                                <?php
                                                //verifica o id da categoria e deixa seleciona no campo
                                                $readCat = read(TAB_CATEGORIA, "WHERE id = '$data[categoria]' ");
                                                if (!mysqli_num_rows($readCat)):
                                                    echo'<option value="">Selecionar</option>';
                                                else:
                                                    //foreach ($readCat as $cate); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                                                    $cate = mysqli_fetch_array($readCat);
                                                    echo'<option value=' . $cate[id] . '>' . $cate[nome] . '</option>';
                                                endif;

                                                //leitura da categoria na tabela categoria
                                                $read = read(TAB_CATEGORIA, "ORDER BY id DESC");
                                                if (!$read):
                                                    echo '<span class="ms no">Oppss! Não existe categoria cadastrada no momento! Por favor, mais tarde!</span>';
                                                else:
                                                    foreach ($read as $cat):
                                                        echo'<option value=' . $cat[id] . '>' . $cat[nome] . '</option>';
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>                                  
                                        </div>			
                                    </div>

                                    <div class="control-group">											
                                        <label class="control-label">Status</label>
                                        <div class="controls">
                                            <select class="span2" name="status">
                                                <option value="">Selecione...</option>
                                                <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                            </select>
                                        </div>				
                                    </div>  

                                    <div class="control-group">											
                                        <label class="control-label">Texto</label>
                                        <div class="controls">
                                            <textarea class="span8" name="texto" value="" rows="10"><?php if (isset($data['texto'])) echo $data['texto']; ?></textarea>
                                        </div> 				
                                    </div> 

                                    <div class="form-actions" style="background: none !important; border: none;">
                                        <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">                                                                                  
                                    </div>
                                </form>
                            </div>                           
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
