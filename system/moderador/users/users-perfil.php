<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="target-1" class="widget">
                        <div class="widget-header">
                            <i class="icon-user"> Perfil</i>
                        </div>
                        <div class="widget-content">
                            <div class="tab-pane">
                                <script type="text/javascript">
                                    //remove a mensagem de cadastrado com sucesso
                                    setTimeout(function () {
                                        $(".ok").fadeOut('slow');
                                    }, 6000);
                                </script>
                                <?php
                                $userId = $_SESSION['autUser']['id'];
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (isset($_POST['enviar'])):
                                    unset($data['enviar']);

                                    //valida os campos
                                    if (empty($data['nome'])):
                                        echo '<span class="ms no">Oppss! Nome está em branco!</span>';
                                    elseif (empty($data['email'])):
                                        echo '<span class="ms no">Oppss! E-mail está em branco!</span>';
                                    elseif (!ValidarEmail($data['email'])):
                                        echo '<span class="ms no">Oppss! E-mail inválido! Por favor, digite um e-mail valido!</span>';
                                    elseif (strlen($_POST['senha']) > 0 and ( strlen($_POST['senha']) < 6 || strlen($_POST['senha']) > 15)):
                                        echo '<span class="ms no">Oppss! Senha deve conter de 6 á 15 caracteres. Por favor, tente novamente para continuar!</span>';
                                    elseif ($data['senha'] != $data['code']):
                                        echo '<span class="ms no">Oppss! Por favor, repita a senha igual o campo senha!</span>';
                                    else:
                                        //usa a funcao para verificar se o email existe no banco de dados e nao deixa cadastrar
                                        $readEmail = read(TAB_USERS, "WHERE email = '$data[email]' AND id != '$userId' ");
                                        if (mysqli_num_rows($readEmail) >= 1):
                                            echo '<span class="ms no">Oppss! Esse perfil não existe, tente novamente!</span>';
                                        else:
                                            //armazena os dados oculdo que o usuario nao cadastro para atualizar no banco
                                            $data['auth'] = $_SESSION['autUser']['id'];
                                            $data['data_update'] = date("Y-m-d H:i:s");
                                            $data['code'] = $data['senha'];
                                            //$data['senha'] = md5($data['code']);
                                            $data['senha'] = hash('sha512', $data['code']); //ALTERADO PARA shar512
                                                                                                                                    
                                            //verifica se a senha esta em branco para nao atualizar no banco
                                            if (empty($data['code'])):
                                                unset($data['senha']);
                                                unset($data['code']);
                                            endif;
                                            
                                            //usa a funcao para fazer update através do id
                                            update(TAB_USERS, $data, "id = '$userId'");
                                            echo C_SALVO;
                                            echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHHOME . '">';
                                        endif;
                                    endif;
                                endif;

                                //usa a funcao para fazer leitura atraves do id
                                $readUser = read(TAB_USERS, "WHERE id='$userId'");
                                if (mysqli_num_rows($readUser) < 1):
                                    echo '<span class="ms no">Oppss! Não existe esse usuario, tente novamente!</span>';
                                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHHOME . '">';
                                else:
                                    foreach ($readUser as $data):
                                        ?>
                                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                            <div class="control-group">
                                                <div style="clear: both;"></div>
                                                <label class="control-label">Nome:</label>
                                                <div class="controls">
                                                    <input type="text" class="span6 disabled" name="nome" value="<?php if (isset($data['nome'])) echo $data['nome']; ?>"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">E-mail:</label>
                                                <div class="controls">
                                                    <input type="text" class="span6 disabled" name="email" value="<?php if (isset($data['email'])) echo $data['email']; ?>">
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Senha:</label>
                                                <div class="controls">
                                                    <input type="password" class="span6 disabled" name="senha">
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Repita a senha:</label>
                                                <div class="controls">
                                                    <input type="password" class="span6 disabled" name="code">
                                                    <p id="resultado" style="color: #ba4a48; font-weight: bold; float: right; margin-right: 180px; font-size: 11px; width: auto; margin-top: 5px; text-align: right;"></p>
                                                </div>
                                            </div>
                                            <div class="form-actions"
                                                 style="background: none !important; border: none;">
                                                <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">
                                            </div>
                                        </form>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
