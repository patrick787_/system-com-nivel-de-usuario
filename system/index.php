<?php
ob_start();
session_start();
require_once('../config/crud.php');
require_once('../config/funcoes.php');

//VERIFICA SE EXISTE UMA SESSÃO DO USUARIO
if (!empty($_SESSION['autUser'])):
    logar();
else:
    ?>
    <!DOCTYPE html>
    <html lang="pt-br">
        <head>
            <meta charset="utf-8">
            <title>System - com nível de usuário</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            <meta name="apple-mobile-web-app-capable" content="yes">
            <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
            <link href="css/font-awesome.css" rel="stylesheet">
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
            <link href="css/style.css" rel="stylesheet" type="text/css">
            <link href="css/pages/signin.css" rel="stylesheet" type="text/css">
        </head>
        <body>
            <div class="account-container">
                <div class="content clearfix">
                    <form action="#" method="post">
                        <h1>Fa&ccedil;a seu Login</h1>
                        <?php
                        $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                        if (!empty($data['login'])):
                            unset($data['login']);
                            
                            if (in_array('', $data)):
                                echo C_BRANCOS;
                            elseif (!ValidarEmail($data['email'])):
                                echo '<span class="ms no">Oppss! E-mail inválido! <br>Por favor, digite um e-mail válido!</span>';
                            elseif (strlen($data['senha']) < 6 || strlen($data['senha']) > 15):
                                echo '<span class="ms no">Oppss! Senha deve conter de 6 á 15 caracteres!</span>';
                            else:
                                //ARMAZENA OS DADOS DO USUARIO
                                $email = $data['email'];
                                //$senha = md5($data['senha']);
								$senha = hash('sha512', $data['senha']); //ALTERADO PARA shar512

                                //EXTRAI OS DADOS DO USUARIO ATRAVES DO E-MAIL
                                $readUser = read(TAB_USERS, "WHERE email = '$email' AND senha = '$senha'");
                                //foreach ($readUser as $autUser); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                                $autUser = mysqli_fetch_array($readUser);

                                //VERIFICA SE O USUARIO DIGITOU O EMAIL E SENHA IGUAL DO BANCO
                                if ($email == $autUser['email'] && $senha == $autUser['senha']):
                                    
                                    if ($autUser['status'] != '1')://VERIFICA SE O USUARIO ESTA ATIVO
                                        echo '<span class="ms no">Oppss! Você não tem permissão! Por favor, entrar em contato com um administrador!</span>';
                                    elseif ($autUser['log'] != '0'):

                                        //RECUPERA OS DADOS O USUARIO ATRAVES DO ID
                                        $readStarTime = read(TAB_USERS, "WHERE id = '$autUser[id]'");
                                        //foreach($readStarTime as $autTime); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                                        $autTime = mysqli_fetch_array($readStarTime);
                                        $dataAtual = date("Y-m-d H:i:s");
                                        
                                        if ($dataAtual <= $autTime['log_in_time']): //VERIFICA A HORA DO USUARIO NO BANCO
                                            echo '<span class="ms no">Oppss! ' . $autUser[nome] . '! Não foi possível logar pois sua conta já esta logada! Por favor, aguarde até as ' . date('H:i:s', strtotime($autTime[log_in_time])) . '</span>';
                                        else:
                                            //ALTERA OS DADOS DO USUARIO NO BANCO PARA LOGAR
                                            $log = array('log_in_time' => date("Y-m-d H:i:s", strtotime('+1 hour')), 'log' => '1', 'log_in' => date("Y-m-d H:i:s"));
                                            update(TAB_USERS, $log, "id = $autUser[id]");

                                            //ARMAZENA A SESSÃO DO USUARIO NO SISTEMA
                                            $_SESSION['autUser'] = $autUser;
                                            header('Location:index.php');											
                                        endif;
                                    else:
                                        //muda os campos da tabela users para log 1 e log_in com a data atual
                                        $log = array('log_in_time' => date("Y-m-d H:i:s", strtotime('+1 hour')), 'log' => '1', 'log_in' => date("Y-m-d H:i:s"));
                                        update(TAB_USERS, $log, "id = $autUser[id]");

                                        //marmazena o log do usuario no sistema
                                        $_SESSION['autUser'] = $autUser;
                                        header('Location:index.php');
                                    endif;
                                else:
                                    echo '<span class="ms no">Oppss! E-mail ou senha não correspondem. Por favor, tente novamente!</span>';
                                endif;
                            endif;
                        endif;

                        //ARMAZENA A MENSAGEM DE LOGOF DO SISTEMA
                        $mg = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);
                        if (!empty($mg)):
                            if ($mg == 'sair'):
                                echo '<span class="ms ok">SESSEÃO ENCERRADA! <br>Obrigado por utilizar nosso sistema!</span>';
                                echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=index.php">';
                            elseif ($mg == 'permissao'):
                                echo '<span class="ms no">Oppss! Você não tem permissão para acessar essa área!</span>';
                                echo '<meta HTTP-EQUIV="refresh" CONTENT="10;URL=' . R_REFRESHHOME . '">';
                            elseif ($mg == 'restrito'):
                                echo '<span class="ms no">Oppss! Acesso negado. Por favor, efetue o login!</span>';
                                echo '<meta HTTP-EQUIV="refresh" CONTENT="10;URL=' . R_REFRESHHOME . '">';
                            endif;
                        endif;
                        ?>
                        <div class="login-fields">
                            <p>Entre com seus dados:</p>
                            <div class="field">
                                <label for="username">E-mail</label>
                                <input type="email" name="email" placeholder="E-mail" class="login username-field" value="<?php if ($data['email']) echo $data['email']; ?>"/>
                            </div>

                            <div class="field">
                                <label for="password">Senha:</label>
                                <input type="password" id="password" name="senha" placeholder="Senha" class="login password-field" value="<?php if ($data['senha']) echo $data['senha']; ?>"/>
                            </div>
                        </div>
                        <div class="login-actions">
                            <input type="submit" name="login" class="button btn btn-success btn-large" value="Logar" class="bt" title="Logar"/>
                        </div>
                    </form>
                </div>
            </div>
            <div class="login-extra" style="text-align: center;">
                Esqueceu a senha? <a href="recover1.php" title="Clique aqui">Recover 1!</a><br>
                Esqueceu a senha? <a href="recover2.php" title="Clique aqui">Recover 2!</a>
            </div>
            <script src="js/jquery-1.7.2.min.js"></script>
            <script src="js/bootstrap.js"></script>
            <script src="js/signin.js"></script>
        </body>
    </html>
<?php
endif;
ob_end_flush();