<div class="container">
    <div class="row">
        <div class="span12">
            <div class="error-container">
                <h1 style="font-size: 6em;">404</h1>
                <div class="error-details" style="font-size: 3em;">
                    P&aacute;gina n&atilde;o encontrada, tente novamente!
                </div>
                <div class="error-actions">
                    <a href="admin-master/index.php" class="btn btn-large btn-primary">
                        <i class="icon-chevron-left"></i>
                        Voltar!
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>