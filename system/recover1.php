<?php
ob_start();
session_start();
require_once('../config/crud.php');
require_once('../config/funcoes.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Recuperar senha - System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.css" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/pages/signin.css" rel="stylesheet" type="text/css">        
    </head>
    <body>
        <div class="account-container">
            <div class="content clearfix">
                <?php
                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                if (!empty($data['enviar'])):
                    unset($data['enviar']);

                    if (empty($data['email'])):
                        echo'<span class="ms no">Oppss! Preencha o campo e-mail!</span>';
                    elseif (!ValidarEmail($data['email'])):
                        echo '<span class="ms no">Oppss! E-mail inválido! <br>Por favor, digite um e-mail válido para continuar!</span>';
                    elseif (trim($_POST['captcha']) != trim(strtolower($_SESSION['codigoCaptcha']))):
                        echo '<span class="ms no">Oppss! Código inválido!<br> Por favor, digite o código novamente!</span>';
                    else:
                        //RECUPERA OS DADOS DO USUARIO ATRAVES DO EMAIL
                        $readUserMail = read(TAB_USERS, "WHERE email = '$data[email]'");

                        //foreach ($readUserMail as $rows); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                        $rows = mysqli_fetch_array($readUserMail);

                        //VERIFICA SE O STATUS É ATIVO NO BANCO
                        if (!$rows['email']):
                            echo '<span class="ms no">Oppss! Esse e-mail não existe. <br>Por favor, coloque um e-mail de cadastro!</span>';
                        elseif ($rows['status'] != '1'):
                            echo '<span class="ms no">Oppss! Não podemos recuperar sua senha, Por favor, entrar em contato com um administrador!</span>';
                        else:
                            //ARMAZENA A MENSAGEM PARA ENVIAR PARA O USUARIO
                            $msSend = '                            
                                <h2>Olá ' . $rows[nome] . ', recupere sua senha!</h2>
                                <p style="font:12px \'Trebuchet MS\', Arial, Helvetica, sans-serif; color:#000;"> Este e-mail foi enviado pelo sistema ' . SITE . ' pois foi requisitado uma recuperação de senha em nosso formulário.</p>                            
                                <hr /><p style="font:bold 14px \'Trebuchet MS\', Arial, Helvetica, sans-serif; color:#069;">Sua senha é: <span style="font:bold 16px Tahoma, Geneva, sans-serif; color:#900;">' . $rows['code'] . '</span></p>
                                <p style="font:bold 15px Tahoma, Geneva, sans-serif; color:#000;"><strong>Atenciosamente Equipe ' . SITE . '.</strong><br> Mensagem enviada em: ' . date('d/m/Y H:i:s') . '</p>                                     
                            ';

                            //DISPARA O E-MAIL ATRAES DA FUNÇÃO
                            enviarEmail('Lembrete de senha', $msSend, MAILUSER, SITE, $rows['email']);
                            echo '<span class="ms ok">Pronto! ' . $rows[nome] . '. Seus dados foram enviados com sucesso para: <strong>' . $rows['email'] . '</strong>.<br> Por favor, verifique sua caixa de entrada e lixo eletrânico!</span>';
                            unset($data);

                            echo '<meta HTTP-EQUIV="refresh" CONTENT="10;URL=' . R_REFRESHHOME . '">';
                        endif;
                    endif;
                endif;
                ?>
                <form action="#" method="post">
                    <h1>Recuperar senha</h1>
                    <div class="login-fields">
                        <p>Digite o e-mail cadastrado no sistema:</p>
                        <div class="field">
                            <label for="username">E-mail</label>
                            <input type="email" name="email" placeholder="E-mail" class="login username-field" value="<?php if ($data['email']) echo $data['email']; ?>"/>
                        </div>

                        <div class="field">
                            <label for="username">Código</label>
                            <input type="text" name="captcha" placeholder="Código" class="login username-captcha" style="width: 100px;"/>
                            <div class="img" style="float: right; margin-top: -35px; margin-right: 25px;"><img src="../captcha/captcha.php" border="0"/></div>
                        </div>
                    </div>
                    <div class="login-actions">
                        <input type="submit" name="enviar" class="button btn btn-primary btn-large" value="Recuperar" class="bt" title="Recuperar"/>
                    </div>
                </form>
            </div>
        </div>
        <div class="login-extra" style="text-align: center;">
            Deseja logar-se? <a href="index.php" title="Clique aqui">Clique aqui</a>
        </div>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/signin.js"></script>
    </body>
    <script type="text/javascript">
        setTimeout(function () {
            $(".ok").fadeOut(2000);
        }, 9000);
    </script>
</html>
<?php
ob_end_flush();
