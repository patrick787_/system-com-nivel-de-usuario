<?php
ob_start();
session_start();
require_once('../config/crud.php');
require_once('../config/funcoes.php');

//ARMAZENA A DATA ATUAL
$data_atual = date('Y-m-d H:i:s');

//DELETA OS CODIGOS VENCIDOS DO RECOVER
$readChave = read(TAB_CODE, "WHERE cod_data_end < '$data_atual'");
foreach ($readChave as $code):
    delete(TAB_CODE, "cod_data_end = '$code[cod_data_end]'");
endforeach;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Recuperar senha - System</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.css" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/pages/signin.css" rel="stylesheet" type="text/css">        
    </head>
    <body>
        <div class="account-container">
            <div class="content clearfix">
                <?php
                $chave = filter_input(INPUT_GET, 'recover', FILTER_DEFAULT);

                //EXTRA OS DADOS DO RECOVER ATRAVES DA CHAVE
                $readCode = read(TAB_CODE, "WHERE cod_chave = '$chave'");
                //foreach ($readCode as $rows); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                $rows = mysqli_fetch_array($readCode);

                if (!$rows['cod_chave']):
                    echo '<span class="ms no">Oppss! Chave inválida ou já expirou!<br> Por favor, gere uma chave para recuperar sua senha!</span>';
                    echo '<meta HTTP-EQUIV="refresh" CONTENT="10;URL=recover2.php">';
                else:

                    $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                    if (!empty($data['enviar'])):
                        unset($data['enviar']);

                        if (in_array('', $data)):
                            echo'<span class="ms no">Oppss! Preencha todos os campos!</span>';
                        elseif ($data['senha'] != $data['code']):
                            echo '<span class="ms no">Oppss! Por favor, repita a senha!</span>';
                        elseif (trim($_POST['captcha']) != trim(strtolower($_SESSION['codigoCaptcha']))):
                            echo '<span class="ms no">Oppss! Código inválido!<br> Por favor, digite o código novamente!</span>';
                        else:
                            //ARAMZENA OS DADOS PARA GRAVAR NO BANCO
                            $data['data_update'] = date('Y-m-d H:i:s');
                            $data['code'] = $data['senha'];
                            //$data['senha'] = md5($data['code']);
                            $data['senha'] = hash('sha512', $data['code']); //ALTERADO PARA shar512
                            $data['log'] = '0';

                            //LIMPA OS CAMPOS PARA NÃO GRAVAR NO BANCO
                            unset($data['captcha']);

                            //ATUALIZA O USUARIO NO BANCO
                            update(TAB_USERS, $data, "id='$rows[cod_userid]'");

                            //RECUPERA OS DADOS DO USUARIO ATRAVES DO ID
                            $readUser = read(TAB_USERS, "WHERE id = '$rows[cod_userid]'");
                            //foreach ($readUser as $autUser); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                            $autUser = mysqli_fetch_array($readUser);

                            //ALTERA O LOG DO USUARIO NO BANCO
                            $log = array('log_in_time' => date("Y-m-d H:i:s", strtotime('+1 hour')), 'log' => '1', 'log_in' => date("Y-m-d H:i:s"));
                            update(TAB_USERS, $log, "id = $autUser[id]");

                            //CRIA A SESSÃO DO USUARIO NO SISTEMA
                            $_SESSION['autUser'] = $autUser;

                            echo '<span class="ms ok">Poronto, ' . $autUser['nome'] . '! <br>Sua senha foi atualizada com sucesso!<br> Por favor, aguarde que você será redirecionado para o painel...</span>';
                            echo '<meta HTTP-EQUIV="refresh" CONTENT="10;URL=index.php">';                            
                            unset($data);
                        endif;
                    endif;
                    ?>
                    <form action="#" method="post">
                        <h1>Recupera sua senha</h1>
                        <div class="login-fields">
                            <p>Por favor, digite sua nova senha nos campos abaixo:</p>

                            <div class="field">
                                <label for="password">Senha:</label>
                                <input type="password" id="password" name="senha" placeholder="Senha" class="login password-field" value="<?php if ($data['senha']) echo $data['senha']; ?>"/>
                            </div>

                            <div class="field">
                                <label for="password">Senha:</label>
                                <input type="password" id="password" name="code" placeholder="Repita a senha" class="login password-field" value="<?php if ($data['code']) echo $data['code']; ?>"/>
                            </div>

                            <div class="field">
                                <label for="username">Código</label>
                                <input type="text" name="captcha" placeholder="Código" class="login username-captcha" style="width: 100px;"/>
                                <div class="img" style="float: right; margin-top: -35px; margin-right: 25px;"><img src="../captcha/captcha.php" border="0"/></div>
                            </div>
                        </div>
                        <div class="login-actions">
                            <input type="submit" name="enviar" class="button btn btn-primary btn-large" value="Recuperar" class="bt" title="Recuperar"/>
                        </div>
                    </form>
                <?php endif; ?>
            </div>
        </div>
        <div class="login-extra" style="text-align: center;">
            Deseja logar-se? <a href="index.php" title="Clique aqui">Clique aqui</a>
        </div>
        <script src="js/jquery-1.7.2.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/signin.js"></script>
    </body>
    <script type="text/javascript">
        setTimeout(function () {
            $(".ok").fadeOut(2000);
        }, 9000);
    </script>
</html>
<?php
ob_end_flush();
