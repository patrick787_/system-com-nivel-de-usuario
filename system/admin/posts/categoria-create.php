<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">	      		
                    <div id="target-1" class="widget">	 
                        <div class="widget-header">
                            <i class="icon-folder-close"> Cadastrar</i>
                        </div>
                        <div class="widget-content">	      				                            
                            <div class="tab-pane">
                                <?php
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (!empty($data['enviar'])):
                                    unset($data['enviar']);

                                    if (in_array('', $data)):
                                        echo C_BRANCOS;
                                    else:
                                        //RECUPERA OS DADOS ATRAVES DO CAMPO NOME
                                        $readCat = read(TAB_CATEGORIA, "WHERE nome = '$data[nome]'");
                                        if (mysqli_num_rows($readCat) >= 1):
                                            echo '<span class="ms no">Oppss! Categoria já existe! Por favor, digite outra categoria!</span>';
                                        else:
                                            //ARMAZENA OS DADOS PARA GRAVAR NO BANCO
                                            $data['auth'] = $_SESSION['autUser']['id'];
                                            $data['data'] = date('Y-m-d H:i:s');

                                            //GRAVA NO BANCO
                                            create(TAB_CATEGORIA, $data);
                                            echo C_SALVO;
                                            unset($data);
                                        endif;
                                    endif;
                                endif;
                                ?>
                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                    <div class="control-group">	
                                        <div style="clear: both;"></div>
                                        <label class="control-label">T&iacute;tulo:</label>
                                        <div class="controls">
                                            <input type="text" class="span6 disabled" name="nome" value="<?php if (isset($data['nome'])) echo $data['nome']; ?>">
                                        </div>				
                                    </div> 

                                    <div class="control-group">											
                                        <label class="control-label">Status</label>
                                        <div class="controls">
                                            <select class="span2" id="exibir"  name="status">
                                                <option value="">Selecione...</option>
                                                <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                            </select>
                                        </div>				
                                    </div>                                    

                                    <div class="form-actions" style="background: none !important; border: none;">
                                        <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">                                                                                  
                                    </div>
                                </form>
                            </div>                        
                        </div>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>