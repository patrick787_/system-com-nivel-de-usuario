<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="widget widget-table action-table">
    <div class="widget-header"><i class="icon-th-list"></i>
        <h3>Listar</h3>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Data de cadastro</th>
                    <th>Categoria</th>
                    <th>Status</th>
                    <th class="td-actions" style="width: 120px;">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php
                //ATIVA E DESATIVA POSTS
                if (!empty($_GET['id'])):
                    $StatuId = $_GET['id'];
                    $acao = $_GET['acao'];

                    switch ($acao):
                        case 'ativar':
                            $acao = array('status' => '1', 'auth' => $userId, 'data_update' => date("Y-m-d H:i:s"));
                            update(TAB_POSTS, $acao, "id = '$StatuId' ");
                            echo '<span class="ms ok">Pronto! Status foi ativado com sucesso!</span>';
                            echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/post-home">';
                            break;
                        case 'bloquear':
                            $acao = array('status' => '2', 'auth' => $userId, 'data_update' => date("Y-m-d H:i:s"));
                            update(TAB_POSTS, $acao, "id = '$StatuId' ");
                            echo '<span class="ms ok">Pronto! Status foi bloqueado com sucesso!</span>';
                            echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/post-home">';
                            break;
                        default:
                            echo '<span class="ms no">Oppss! Ação não indentificado pelo sistema. Por favor, tente novamente!</span>';
                            echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/post-home">';
                    endswitch;
                endif;

                //DELETA POSTS
                if (!empty($_GET['delid'])):
                    $delId = $_GET['delid'];
                    $thumb = $_GET['thumb'];
                    $pasta = '../../uploads/';

                    //REMOVE A IMAGEM DA PASTA
                    if (file_exists($pasta . $thumb) && !is_dir($pasta . $thumb)):
                        unlink($pasta . $thumb);
                    endif;

                    //DELETA DO BANCO
                    delete(TAB_POSTS, "id = '$delId'");
                    echo C_DELETADO;
                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/post-home">';
                endif;

                //RECUPERA OS DADOS
                $readUser = read(TAB_POSTS, "ORDER BY id DESC");
                if (!$readUser):
                    echo '<span class="ms no">Oppss! Post não existe! Por favor, tente mais tarde!</span>';
                    echo '<meta HTTP-EQUIV="refresh" CONTENT="5;URL=' . R_REFRESHPOSTS . '/post-create">';
                else:
                    foreach ($readUser as $rows):
                        $ico = ($rows['status'] == 1 ? 'active' : 'inactive');
                        //$status = ($rows['status'] == 1 ? 'Ativo' : 'Bloqueado');
                        
                        //RECUPERA AS ATEGORIA ATRAVES DO $rows[categoria]
                        $readCat = read(TAB_CATEGORIA, "WHERE id = '$rows[categoria]'");
                        //foreach ($readCat as $catname); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
                        $catname = mysqli_fetch_array($readCat);
                        ?>
                        <tr>
                            <td><?= $rows['titulo']; ?></td>
                            <td><?= date('d/m/Y H:i:s', strtotime($rows['data'])); ?></td>
                            <td><?= $catname['nome'] ?></td>
                            <td class="<?= $ico; ?>"><?= FuncStatus($rows['status']); ?></td>
                            <td class="td-actions">
                                <a href="<?= R_REFRESHPOSTS; ?>/post-update&id=<?= $rows['id']; ?>" title="Editar" class="btn btn-small btn-success">
                                    <i class="btn-icon-only icon-edit"></i>
                                </a>
                                <a href="<?= R_REFRESHPOSTS; ?>/post-home&delid=<?= $rows['id']; ?>&thumb=<?= $rows['thumb'] ?>" title="Deletar" class="btn btn-danger btn-small">
                                    <i class="btn-icon-only icon-remove"></i>
                                </a>
                                <?php
                                if ($rows['status'] == 1):
                                    echo'<a href="' . R_REFRESHPOSTS . '/post-home&id=' . $rows['id'] . '&acao=bloquear" title="Bloquear" class="btn btn-small">
                                        <i class="btn-icon-only icon-eye-open"></i>
                                    </a>';
                                else:
                                    echo'<a href="' . R_REFRESHPOSTS . '/post-home&id=' . $rows['id'] . '&acao=ativar" title="Ativar" class="btn btn-small">
                                        <i class="btn-icon-only icon-eye-close"></i>
                                    </a>';
                                endif;
                                ?>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
</div>