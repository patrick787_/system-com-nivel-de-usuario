<?php
ob_start();
session_start();
require_once('../../config/crud.php');
require_once('../../config/funcoes.php');

//ARAMZENA O ID DO USUARIO
$userId = $_SESSION['autUser']['id'];

//FUNCAO QUE CAPTURA O LOGO DO USUARIO
SaveLog();

//ATUALIZA O LOG_IN_TIME DO USUARIO NO BANCO
starTime();

//VERIFICA A SESSÃO DO USURIO
if (empty($_SESSION['autUser'])):
    header('Location:' . SYSTEM . '/index.php?exe=restrito');
    die;
endif;

//RECUPERA OS DADOS DO USUARIO
$readUser = read(TAB_USERS, "WHERE id = '$userId'");
//foreach ($readUser as $user); ATUALIZADO PARA MYSQLI_FETCH_ARRAY
$user = mysqli_fetch_array($readUser);

//VERIFICA O STATUS DO USUARIO NO SISTEMA
if ($user['status'] != '1'):
    unset($_SESSION['autUser']);
    header('Location:' . SYSTEM . '/index.php?exe=permissao');
    die;
endif;

//VERIFICA SE O USUARIO ESTA LOGADO
if ($user['log'] != '1'):
    unset($_SESSION['autUser']);
    header('Location:' . SYSTEM . '/index.php?exe=permissao');
    die;
endif;

//VERIFICA SE O NIVEL DE USUARIO TEM PERMISSÃO
//if($user['nivel'] != '1'):
//logar();
//endif;

//VERIFICA SE O NIVEL DO USUARIO TEM PERMISSAO PARA ACESSAR ESTA AREA
if ($user['nivel'] < '1' || $user['nivel'] > '2'):
    unset($_SESSION['autUser']);
    header('Location:' . SYSTEM . '/index.php?exe=permissao');
    die;
endif;

//LOGOFF NO SISTEMA
$sair = filter_input(INPUT_GET, 'sair', FILTER_VALIDATE_BOOLEAN);
if ($sair):
    
    //ATUALIZA O USUARIO PARA DESLOGAR O MESMO
    $logout = array('log' => '0', 'log_out' => date("Y-m-d H:i:s"));
    update(TAB_USERS, $logout, "id = $user[id]");

    unset($_SESSION['autUser']);
    header('Location:' . SYSTEM . '/system/index.php?exe=sair');
    die;
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Sistema de Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
        <link href="../css/font-awesome.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/pages/dashboard.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
        require_once('topo.php');
        require_once('menu.php');
        ?>
        <div class="main">
            <div class="main-inner">
                <div class="container">
                    <div class="row">
                        <?php
                        //if (empty($_GET['exe'])):
                        //require_once('../home.php');
                        //elseif (file_exists($_GET['exe'] . '.php')):
                        //require_once($_GET['exe'] . '.php');
                        //else:
                        //require_once('../error.php');
                        //endif;
                        
                        //ATUALIZADO PARA OS FILTROS DO PHP						
                        $getexe = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);
                        if (empty($getexe)):
                            require('../home.php');
                        elseif (file_exists($getexe . '.php')):
                            require_once($getexe . '.php');
                        else:
                            require_once('../error.php');
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        require('../footer.php');
        ?>
        <!-- MicEdit -->
        <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
        <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

        <script src="../js/jquery-1.7.2.min.js"></script>
        <script src="../js/excanvas.min.js"></script>
        <script src="../js/chart.min.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src=../js/full-calendar/fullcalendar.min.js"></script>
        <script src="../js/base.js"></script>
    </body>
</html>
<?php
ob_end_flush();
