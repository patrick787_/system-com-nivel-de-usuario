<?php
//VERIFICA A SESSÃO DO USUARIO
if (empty($_SESSION['autUser'])):
    header('Location: ../index.php');
endif;
?>
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="target-1" class="widget">
                        <div class="widget-header">
                            <i class="icon-user"> Cadastrar</i>
                        </div>
                        <div class="widget-content">
                            <div class="tab-pane">
                                <?php
                                $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

                                if (!empty($data['enviar'])):
                                    unset($data['enviar']);

                                    if (in_array('', $data)):
                                        echo C_BRANCOS;
                                    elseif (!ValidarEmail($data['email'])):
                                        echo '<span class="ms no">Oppss! E-mail inválido! Por favor, digite um e-mail valido!</span>';
                                    elseif (strlen($data['senha']) < 6 || strlen($data['senha']) > 15):
                                        echo '<span class="ms no">Oppss! Senha deve conter de 6 á 15 caracteres. Por favor, tente novamente para continuar!</span>';
                                    elseif ($data['senha'] != $data['code']):
                                        echo '<span class="ms no">Oppss! Por favor, repita a senha igual o campo senha!</span>';
                                    else:
                                        //RECUPERA OS DADOS ATRAVES DO $data[email]
                                        $readUser = read(TAB_USERS, "WHERE email = '$data[email]'");
                                        if (mysqli_num_rows($readUser) >= 1):
                                            echo '<span class="ms no">Oppss! E-mail já existe no sistema. Por favor, cadastre um e-mail diferente!</span>';
                                        else:
                                            //ARMAZENA OS DADOS PARA GRAVAR NO BANCO
                                            $data['auth'] = $_SESSION['autUser']['id'];
                                            $data['data'] = date("Y-m-d H:i:s");
                                            $data['nivel'] = '3';
                                            $data['code'] = $data['senha'];
                                            //$data['senha'] = md5($data['code']);
                                            $data['senha'] = hash('sha512', $data['code']); //ALTERADO PARA shar512

                                            //GRAVA NO BANCO
                                            create(TAB_USERS, $data);
                                            echo C_SALVO;
                                            unset($data);
                                        endif;
                                    endif;
                                endif;
                                ?>

                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" style="padding-top: 20px;">

                                    <div class="control-group">
                                        <div style="clear: both;"></div>
                                        <label class="control-label">Nome:</label>
                                        <div class="controls">
                                            <input type="text" class="span6 disabled" name="nome" value="<?php if (isset($data['nome'])) echo $data['nome']; ?>"/>
                                        </div> 
                                    </div> 

                                    <div class="control-group">
                                        <label class="control-label">E-mail:</label>
                                        <div class="controls">
                                            <input type="text" class="span6 disabled" name="email" value="<?php if (isset($data['email'])) echo $data['email']; ?>">
                                        </div>
                                    </div> 

                                    <div class="control-group">
                                        <label class="control-label">Senha:</label>
                                        <div class="controls">
                                            <input type="password" class="span6 disabled" name="senha" value="<?php if (isset($data['senha'])) echo $data['senha']; ?>">
                                        </div> 
                                    </div> 

                                    <div class="control-group">
                                        <label class="control-label">Repita a senha:</label>
                                        <div class="controls">
                                            <input type="password" class="span6 disabled" name="code" value="<?php if (isset($data['code'])) echo $data['code']; ?>">
                                            <p id="resultado" style="color: #ba4a48; font-weight: bold; float: right; margin-right: 180px; font-size: 11px; width: auto; margin-top: 5px; text-align: right;"></p>
                                        </div> 
                                    </div> 

                                    <div class="control-group">
                                        <label class="control-label">Status</label>
                                        <div class="controls">
                                            <select class="span2" name="status">
                                                <option value="">Selecione...</option>
                                                <option value="1" <?php if (isset($data['status']) && $data['status'] == 1) echo 'selected="selected"'; ?>>Ativo</option>
                                                <option value="2" <?php if (isset($data['status']) && $data['status'] == 2) echo 'selected="selected"'; ?>>Bloqueado</option>
                                            </select>
                                        </div> 
                                    </div> 

                                    <div class="form-actions" style="background: none !important; border: none;">
                                        <input type="submit" name="enviar" class="btn btn-primary" value="Salvar">
                                    </div> 
                                </form>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>